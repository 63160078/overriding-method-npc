/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.overridingmethod;

/**
 *
 * @author Ow
 */
public class NPC {
    
    protected String name;
    protected int age;
    protected String food;
    
    public NPC(String name,int age,String food) {
        System.out.println("NPC Created");
        this.name = name;
        this.age = age;
        this.food = food; 
    }
     
    public void sayThanks() {
        System.out.println("NPC says Thank you");
    }
    
    public void sayTasty() {
        System.out.println("NPC say Tasty !!");
    }
    
}
