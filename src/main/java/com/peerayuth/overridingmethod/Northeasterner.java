/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.overridingmethod;

/**
 *
 * @author Ow
 */
public class Northeasterner extends NPC{
    
    public Northeasterner(String name,int age,String food) {
        super(name,age,food);
        System.out.println("NPC from Northeastern created");
    }
    
    @Override
    public void sayThanks() {
        super.sayThanks();
        System.out.println(name + " : Kob jai lai");
    }
    
    @Override
    public void sayTasty() {
        super.sayTasty();
        System.out.println(name + " : Zaab e lee ~");
    }
    
}
