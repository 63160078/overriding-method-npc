/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.overridingmethod;

/**
 *
 * @author Ow
 */
public class Northerner extends NPC {
    
    public Northerner(String name,int age,String food) {
        super(name,age,food);
        System.out.println("NPC from Northern created");
    }
    
    @Override
    public void sayThanks() {
        super.sayThanks();
        System.out.println(name + " : Yin dee jaw");
    }
    
    @Override
    public void sayTasty() {
        super.sayTasty();
        System.out.println(name + " : Lum ka nad ~");
    }
    
    
}
