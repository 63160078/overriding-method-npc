/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.peerayuth.overridingmethod;

/**
 *
 * @author Ow
 */
public class TestNPC {
    public static void main(String[] args) {
        
        Northerner npc1 = new Northerner("Nuea",25,"Nam prik num");
        npc1.sayThanks();
        npc1.sayTasty();
        
        System.out.println("");
        
        Southerner npc2 = new Southerner("Noi",23,"Kaeng tai pla");
        npc2.sayThanks();
        npc2.sayTasty();

        System.out.println("");
        
        Northeasterner npc3 = new Northeasterner("Duang",23,"Somtum");
        npc3.sayThanks();
        npc3.sayTasty();
        
    }
    
}
